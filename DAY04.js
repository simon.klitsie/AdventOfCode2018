
var fs = require('fs');

var DataSet = [];

var array = fs.readFileSync('data_DAY04.txt').toString().split("\r\n");

// populate DataSet

for(i in array) 
{
    //[1518-11-11 00:18] wakes up
    //[1518-10-22 00:26] falls asleep
    //[1518-07-24 23:47] Guard #3319 begins shift

    var t1 = array[i].substr(1,4);

    var t2 = array[i].substr(6,2);

    var t3 = array[i].substr(9,2);

    var t4 = array[i].substr(12,2);

    var t5 = array[i].substr(15,2);

    var _GuardID;
    var _action;

    if(/^.*Guard #.*$/.test(array[i]) === true)
    {
        var rx = /^.*(\s)Guard(\s)#(?<id>\d*)(\s)begins(\s)shift$/;
        var result = rx.exec(array[i]);       

        _GuardID = result.groups.id;
        _action = null;
    }

    if(/^.*falls asleep$/.test(array[i]) === true)
    {
        _GuardID = null;
        _action = "falls asleep";  
    }

    if(/^.*wakes up$/.test(array[i]) === true)
    {
        _GuardID = null;
        _action = "wakes up";  
    }

    DataSet.push({
        FullString: array[i],
        GuardID: _GuardID,
        year: parseInt(t1),
        month: parseInt(t2),
        day: parseInt(t3),
        hour: parseInt(t4),
        minute: parseInt(t5),
        action: _action
    });
}

DataSet.sort();

var date2 = new Date('1995-12-17T03:24:00');


DataSet.sort(function(a, b){
    var keyA = new Date(a.year, a.month, a.day, a.hour, a.minute, 0),
        keyB = new Date(b.year, b.month, b.day, b.hour, b.minute, 0);
    // Compare the 2 dates
    if(keyA < keyB) return -1;
    if(keyA > keyB) return 1;
    return 0;
});


var GuardList = [];

var CurrentGuard;

var FallsAsleepTime;

for(item in DataSet)
{
    if(DataSet[item].GuardID !== null)
    {
        CurrentGuard = GuardList.find(x => x.GuardID == DataSet[item].GuardID);

        if(CurrentGuard == null)
        {
            CurrentGuard = { GuardID: DataSet[item].GuardID, minutescounter: 0, minutes: [] };

            GuardList.push(CurrentGuard);
        }                    
    }

    if(DataSet[item].action != null)
    {
        if(DataSet[item].action === "falls asleep")
        {
            FallsAsleepTime = new Date(DataSet[item].year, DataSet[item].month, DataSet[item].day, DataSet[item].hour, DataSet[item].minute, 0);
        }

        if(DataSet[item].action === "wakes up")
        {
            var WakeUpTime = new Date(DataSet[item].year, DataSet[item].month, DataSet[item].day, DataSet[item].hour, DataSet[item].minute, 0);

            var diff = Math.abs(WakeUpTime - FallsAsleepTime);
            var diffMinutes2 = Math.floor(diff/1000/60);


            CurrentGuard.minutescounter = CurrentGuard.minutescounter + diffMinutes2;

            var tempDate2 = new Date(FallsAsleepTime);

            for(var m = 0;m < diffMinutes2; m++)
            {
                tempDate2.setMinutes(FallsAsleepTime.getMinutes() + m);

                var currentMinute2 = CurrentGuard.minutes.find(x => x.min == tempDate2.getMinutes());

                if(currentMinute2 != null)
                {
                    currentMinute2.counter = currentMinute2.counter + 1;
                }
                else
                {
                    CurrentGuard.minutes.push({
                        min: tempDate2.getMinutes(),
                        counter: 1
                    });
                }
            }
        }            
    }    

    // console.log(DataSet[item].FullString);
}


var MaxMinutes = Math.max.apply(Math, GuardList.map(function(o) { return o.minutescounter; }));

var MaxGuard = GuardList.find(x => x.minutescounter == MaxMinutes);

console.log("MaxGuard #" + MaxGuard.GuardID + " - TOTAL MINUTES: " + MaxMinutes);

// =============== PART TWO =============================
console.log("PART TWO =========================");


var max = 0;
var min;
var maxG;

for(item in GuardList)
{
    var guard = GuardList[item];

    var MaxMinutesCounter = Math.max.apply(Math, GuardList[item].minutes.map(function(o) { return o.counter; }));

    if(MaxMinutesCounter > max)
    {
        max = MaxMinutesCounter;

        maxG = GuardList[item].GuardID;

        min = GuardList[item].minutes.find(o => o.counter == MaxMinutesCounter);
    }
}


console.log("max GuardID: " + maxG + " - min: " + min.min + " frequency: " + max);



console.log("PART TWO =========================");
// ======================================================




var CurrentGuardID;

var Minutes = [];


for(item in DataSet)
{
    if(DataSet[item].GuardID !== null)
    {
        CurrentGuardID = DataSet[item].GuardID
    }

    if((DataSet[item].action != null) && (CurrentGuardID === "179"))
    {
        //console.log(DataSet[item].FullString);

        if(DataSet[item].action === "falls asleep")
        {
            FallsAsleepTime = new Date(DataSet[item].year, DataSet[item].month, DataSet[item].day, DataSet[item].hour, DataSet[item].minute, 0);
        }

        if(DataSet[item].action === "wakes up")
        {
            var WakeUpTime = new Date(DataSet[item].year, DataSet[item].month, DataSet[item].day, DataSet[item].hour, DataSet[item].minute, 0);

            var diff = Math.abs(WakeUpTime - FallsAsleepTime);

            var diffMinutes = Math.floor(diff/1000/60);

            //console.log("diffMinutes " + diffMinutes);

            var tempDate = new Date(FallsAsleepTime);

            for(var i = 0; i < diffMinutes;i++)
            {
                tempDate.setMinutes(FallsAsleepTime.getMinutes() + i);

                var currentMinute = Minutes.find(x => x.min == tempDate.getMinutes());

                if(currentMinute != null)
                {
                    currentMinute.counter = currentMinute.counter + 1;
                }
                else
                {
                    Minutes.push({
                        min: tempDate.getMinutes(),
                        counter: 1
                    });
                }

                //console.log("tempDate " + tempDate);
            }
        }            
    }      
}


var MaxM = Math.max.apply(Math, Minutes.map(function(o) { return o.counter; }));

//console.log("MaxM " + MaxM);

var MaxTimeStamp = Minutes.find(x => x.counter == MaxM);

console.log("MaxTimeStamp minute: " + MaxTimeStamp.min);


for(item in Minutes)
{
    ///console.log("Minutes  " + Minutes[item].min + " - " + Minutes[item].counter);
}
