
var fs = require('fs');
var round = Math.round;

var DataSet = [];

var array = fs.readFileSync('data_DAY01.txt').toString().split("\n");

for(i in array) 
{
    var line = array[i];

    DataSet.push({
                    operator: line.substring(0,1),
                    value: line.substring(1,line.len)
                });
}

// PART ONE - get total

var Frequency = 0;

for(i in DataSet) 
{
    if(DataSet[i].operator == "+")
    {
        Frequency = Frequency + round(DataSet[i].value);
    }

    if(DataSet[i].operator == "-")
    {
        Frequency = Frequency - round(DataSet[i].value);
    }
}

console.log("Resulting Frequency: " + Frequency);


// PART TWO - DO other stuff


var counter = 1;

var FoundSolution = false;

var RunningFrequency = 0;

var IterationFrequencyList = [];

IterationFrequencyList.push(RunningFrequency);

while(FoundSolution === false)
{
    counter = counter + 1;

    for(i in DataSet) 
    {
        if(DataSet[i].operator == "+")
        {
            RunningFrequency = RunningFrequency + round(DataSet[i].value);
        }

        if(DataSet[i].operator == "-")
        {
            RunningFrequency = RunningFrequency - round(DataSet[i].value);
        }


        if(IterationFrequencyList.includes(RunningFrequency))
        {
            console.log('Found Solution! ' + RunningFrequency);
            FoundSolution = true;            
        }

        IterationFrequencyList.push(RunningFrequency);
    }



    // read lines from file and check if we found the answer in historic iterations

    var FileStorageArray = fs.readFileSync('message.txt').toString().split("|");

    for(i in FileStorageArray)
    {
        if(FileStorageArray[i] !== "")
        {                    
            var StoredArray = JSON.parse(FileStorageArray[i]);

            for(j in IterationFrequencyList)
            {
                if(StoredArray.includes(IterationFrequencyList[j]))
                {
                    console.log('Found Solution! ' + IterationFrequencyList[j]);
                    FoundSolution = true;
                }
            }
        }

    } 


    // save IterationFrequencyList to file 

    fs.appendFileSync('message.txt', JSON.stringify(IterationFrequencyList) + "|", function (err) {
        if (err) throw err;
        console.log('Saved!');
    });
    

    // clear the list 

    IterationFrequencyList = [];


    // test

    if(counter === 500)
    {
        FoundSolution = true;
    }

}





